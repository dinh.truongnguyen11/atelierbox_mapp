﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AtelierBoxVendingMachineApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public List<BlockUI> BlockProductUIs { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            LoadProduct();
        }


        public void LoadProduct(bool isRetireve = true)
        {
            List<Product> listProduct = new List<Product> {
                new Product { Name = "BOTTLED DRINK WATER1" },
                new Product { Name = "BOTTLED DRINK WATER2" },
                new Product { Name = "BOTTLED DRINK WATER3" },
                new Product { Name = "BOTTLED DRINK WATER4" },
                new Product { Name = "BOTTLED DRINK WATER5" },
                new Product { Name = "BOTTLED DRINK WATER6" },
                new Product { Name = "BOTTLED DRINK WATER7" },
                new Product { Name = "BOTTLED DRINK WATER8" },
                new Product { Name = "BOTTLED DRINK WATER9" },
                new Product { Name = "BOTTLED DRINK WATER10" },
                new Product { Name = "BOTTLED DRINK WATER11" },
                new Product { Name = "BOTTLED DRINK WATER12" },
            };


            List<BlockUI> BlockUIs = new List<BlockUI>();

            int i = 0;
            BlockUIs = (from lp in listProduct
                        group lp by i++ % 2 into ll
                        select new BlockUI { block_id = ll.Key, ProductUIs = ll.ToList() }).ToList();

            BlockProductUIs = BlockUIs;

            // Bidding data to XAML
            this.DataContext = this;
        }
    }
}
