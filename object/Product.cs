﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

public class Product 
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Decription { get; set; }
    public DateTime DateCreate { get; set; }
    public string UserCreate { get; set; }
    public string Active { get; set; }
    public string Quantity { get; set; }
    public string Price { get; set; }

}


